/**
    * Load all toggles
    */
    loadToggle() {
        // define the button class
        let theButton = $('.toggleButton-js');
        // define the toggle container class
        let toggleContainer = $('.toggle-js');

        // on click,
        theButton.on('click', ({currentTarget}) => {
            // on any other button instance
            theButton.not($(currentTarget))
                // remove the active class
                .removeClass('active')
                // and on the following container
                .next(toggleContainer)
                // remove that active too
                .slideToggle();
            //apply an active class to the button clicked
            $(currentTarget).toggleClass('active')
                // the next toggle Container
                .next(toggleContainer)
                // toggle class active
                .slideToggle();
        })
    }